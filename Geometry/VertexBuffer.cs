﻿using System.Collections.Generic;
using UnityEngine;

namespace CoreDopamine.Geometry
{
    public class VertexBuffer : List<Vector3>
    {
        public VertexBuffer(int vertexCount) : base()
        {

            for(var vertexIndex = 0; vertexIndex < vertexCount; vertexIndex++)
            {
                Add(Vector3.zero);
            }
        }

        public void CopyFromVertexBuffer(VertexBuffer vertexBuffer)
        {
            for(var vertexIndex = 0; vertexIndex < vertexBuffer.Count; vertexIndex++)
            {
                var vertexToCopy = vertexBuffer[vertexIndex];
                this[vertexIndex].Set(vertexToCopy.x, vertexToCopy.y, vertexToCopy.z);
            }
        }

        public void SetVertex(int vertexIndex, float x = 0, float y = 0, float z = 0)
        {
            this[vertexIndex] = new Vector3(x, y, z);
        }

        public void SetVertex(int vertexIndex, Vector3 vertex)
        {
            this[vertexIndex].Set(vertex.x, vertex.y, vertex.z);
        }

        public void SetVertexX(int vertexIndex, float x)
        {
            var vertex = this[vertexIndex];
            this[vertexIndex].Set(x, vertex.y, vertex.z);
        }

        public void SetVertexY(int vertexIndex, float y)
        {
            var vertex = this[vertexIndex];
            this[vertexIndex].Set(vertex.x, y, vertex.z);
        }

        public void SetVertexZ(int vertexIndex, float z)
        {
            var vertex = this[vertexIndex];
            this[vertexIndex].Set(vertex.x, vertex.y, z);
        }

        public void TransformVertices(Transform transform)
        {
            for(var vertexIndex = 0; vertexIndex < this.Count; vertexIndex++)
            {
                this[vertexIndex] = transform.TransformPoint(this[vertexIndex]);
            }
        }

        public Vector2[] GetAsVector2Array()
        {
            var vec2List = new List<Vector2>(Count);

            for(var vertexIndex = 0; vertexIndex < Count; vertexIndex++ )
            {
                vec2List.Add(this[vertexIndex]);
            }

            return vec2List.ToArray();
        }
    }
}

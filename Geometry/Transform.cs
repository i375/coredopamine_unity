﻿using System.Collections.Generic;
using UnityEngine;

namespace CoreDopamine.Geometry
{
    public class Transform
    {
        struct SRT
        {
            public Vector3 position;
            public Quaternion rotation;
            public Vector3 scale;
        }
        
        private Matrix4x4 matrix4x4;

        private SRT srt;

        private SRT[] srtStack;
        private const int srtStackSize = 10;
        private int srtStackPointer = 0;

        public Transform()
        {
            srt = new SRT();

            srtStack = new SRT[srtStackSize];

            srt.position = Vector3.zero;
            srt.scale = Vector3.one;
            srt.rotation = Quaternion.identity;

            UpdateMatrix();
        }

        public void PushTransform()
        {
            srtStack[srtStackPointer] = srt;
            srtStackPointer++;
        }

        public void PopTransform()
        {
            srtStackPointer--;
            srt = srtStack[srtStackPointer];
        }

        public virtual void SetPosition(float x, float y, float z)
        {
            srt.position.Set(x, y, z);

            UpdateMatrix();
        }

        public virtual void SetPosition(Vector3 position)
        {
            srt.position = position;
            UpdateMatrix();
        }

        public virtual void SetScale(float x, float y, float z)
        {
            srt.scale.Set(x, y, z);

            UpdateMatrix();
        }

        public virtual void SetRotationDegreesAroundAngles(float x, float y, float z)
        {
            srt.rotation = Quaternion.Euler(x, y, z);

            UpdateMatrix();
        }

        public virtual void SetRotationRadiansAroundAngles(float x, float y, float z)
        {
            srt.rotation = Quaternion.Euler(x * Mathf.Rad2Deg, y * Mathf.Rad2Deg, z * Mathf.Rad2Deg);

            UpdateMatrix();
        }

        public Vector3 TransformPoint(Vector3 point)
        {
            return matrix4x4.MultiplyPoint(point);
        }

        public Vector3 TransformPointInverse(Vector3 point)
        {
            return matrix4x4.inverse.MultiplyPoint(point);
        }

        public void TransformVertices(ref VertexBuffer vertexBuffer)
        {
            for(var vertexIndex =0; vertexIndex < vertexBuffer.Count; vertexIndex++)
            {
                vertexBuffer[vertexIndex] = TransformPoint(vertexBuffer[vertexIndex]);
            }
        }

        public void TransformVerticesInverse(ref VertexBuffer vertexBuffer)
        {
            for (var vertexIndex = 0; vertexIndex < vertexBuffer.Count; vertexIndex++)
            {
                vertexBuffer[vertexIndex] = TransformPointInverse(vertexBuffer[vertexIndex]);
            }
        }

        public Vector3 GetPosition()
        {
            return srt.position;
        }

        public Vector3 GetRotationEulerAngles()
        {
            return srt.rotation.eulerAngles;
        }

        public Vector3 GetScale()
        {
            return srt.scale;
        }

        private void UpdateMatrix()
        {
            matrix4x4.SetTRS(
                srt.position,
                srt.rotation,
                srt.scale);
        }
    }

}
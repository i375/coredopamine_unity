﻿Shader "CoreDopamine/SolidColor"
{
    Properties
    {
    _SolidColor("SolidColor", Color) = (0,0,0,1)
    }
    SubShader
    {
        Tags{ "RenderType" = "Opaque" }
        LOD 100

        Pass
        {
            CGPROGRAM
            #pragma vertex vertexShader
            #pragma fragment fragmentShader

            #include "UnityCG.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
            };

            struct v2f
            {
                float4 vertex : SV_POSITION;
            };

            float4 _SolidColor;

            v2f vertexShader(appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                return o;
            }

            float4 fragmentShader(v2f i) : COLOR
            {
                return _SolidColor;
            }

             ENDCG
        }
    }
}
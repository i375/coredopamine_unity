﻿// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

Shader "CoreDopamine/VertexColorShader"
{
	Properties
	{
	}
	SubShader
	{
		Tags { "RenderType"="Opaque" }
        Lighting Off
		LOD 100

		Pass
		{
            

			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag

			
			#include "UnityCG.cginc"

			struct VertexData
			{
				float4 pos : POSITION;
                float4 color : COLOR;
			};

			struct Vertex2Fragment
			{
                float4 color : COLOR;
				float4 pos : SV_POSITION;
			};
			
            Vertex2Fragment vert (VertexData vertexData)
			{
                Vertex2Fragment o;

				o.pos = UnityObjectToClipPos(vertexData.pos);
                o.color = vertexData.color;

				return o;
			}
			
            float4 frag (Vertex2Fragment i) : SV_Target
			{
				return i.color;
			}
			ENDCG
		}
	}
}

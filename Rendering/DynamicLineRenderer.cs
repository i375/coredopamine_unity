﻿using UnityEngine;
using System.Collections;

namespace CoreDopamine.Rendering
{

    public class DynamicLineRenderer
    {

        private Vector3[] vertices;
        private Color[] colors;
        private int[] vertexIndices;

        private int vertexIndex;
        private Color drawColor;

        private Mesh lineMesh;

        private Material lineMaterial;

        public DynamicLineRenderer(int pointCount)
        {
            InitBuffers(pointCount);

            color = new Color(1, 1, 1, 1);

            vertexIndex = 0;
            drawColor = new Color();

            lineMesh = new Mesh();
            
            lineMaterial = new Material(Shader.Find("CoreDopamine/VertexColorShader"));
        }

        void InitBuffers(int pointCount)
        {
            vertices = new Vector3[pointCount];
            colors = new Color[pointCount];
            vertexIndices = new int[pointCount];

            for (var elementIndex = 0; elementIndex < pointCount; elementIndex++)
            {
                vertices[elementIndex] = new Vector3();
                colors[elementIndex] = new Color();

                vertexIndices[elementIndex] = elementIndex;
            }
        }

        public void Begin()
        {
            vertexIndex = 0;

        }

        private Color color;

        public void SetColor(float r, float g, float b, float a)
        {
            color.r = r;
            color.g = g;
            color.b = b;
            color.a = a;

            SetColor(color);
        }

        public void SetColor(Color color)
        {
            drawColor.r = color.r;
            drawColor.g = color.g;
            drawColor.b = color.b;
            drawColor.a = color.a;
        }

        private void InsertVertex3D(float x, float y, float z)
        {
            colors[vertexIndex].r = drawColor.r;
            colors[vertexIndex].g = drawColor.g;
            colors[vertexIndex].b = drawColor.b;
            colors[vertexIndex].a = drawColor.a;

            vertices[vertexIndex].Set(x, y, z);

            vertexIndex++;
        }

        public void DrawLine(
            float x1, float y1, float z1,
            float x2, float y2, float z2)
        {
            InsertVertex3D(x1, y1, z1);
            InsertVertex3D(x2, y2, z2);
        }

        public void Finish()
        {
            for (var unusedVertexIndex = vertexIndex; unusedVertexIndex < vertices.Length; unusedVertexIndex++)
            {
                vertices[unusedVertexIndex].Set(0F, 0F, 0F);
            }
        }

        private Vector3 minVector3 = new Vector3(-100000, -10000);
        private Vector3 maxVector3 = new Vector3(100000,100000);

        public void Render()
        {
            lineMesh.vertices = vertices;
            lineMesh.colors = colors;
            lineMesh.SetIndices(vertexIndices, MeshTopology.Lines, 0);

            lineMesh.RecalculateBounds();

            Graphics.DrawMesh(lineMesh, Matrix4x4.identity, lineMaterial, 0);
        }

    }

}


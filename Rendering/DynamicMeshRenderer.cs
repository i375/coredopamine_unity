﻿using System;
using UnityEngine;

namespace CoreDopamine.Rendering
{

    public class DynamicMeshRenderer
    {
        private Vector3[] vertices;
        private Color[] colors;
        private int[] vertexIndices;

        private int vertexIndex;
        private Color drawColor;

        private Mesh mesh;

        private Material lineMaterial;

        private ZigZag2DBuilder zigZag2DBuilder;

        public DynamicMeshRenderer(int pointCount)
        {

            InitBuffers(pointCount);
            
            color = new Color(1, 1, 1, 1);

            vertexIndex = 0;
            drawColor = new Color();

            mesh = new Mesh();

            lineMaterial = new Material(Shader.Find("CoreDopamine/VertexColorShader"));
            
        }

        void InitBuffers(int pointCount)
        {
            vertices = new Vector3[pointCount];
            colors = new Color[pointCount];
            vertexIndices = new int[pointCount];

            for (var elementIndex = 0; elementIndex < pointCount; elementIndex++)
            {
                vertices[elementIndex] = new Vector3();
                colors[elementIndex] = new Color();

                vertexIndices[elementIndex] = elementIndex;
            }

            zigZag2DBuilder = new ZigZag2DBuilder(pointCount);
        }

        public void Begin()
        {
            vertexIndex = 0;

        }

        private Color color;

        public void SetColor(float r, float g, float b, float a)
        {
            color.r = r;
            color.g = g;
            color.b = b;
            color.a = a;

            SetColor(color);
        }

        public void SetColor(Color color)
        {
            drawColor.r = color.r;
            drawColor.g = color.g;
            drawColor.b = color.b;
            drawColor.a = color.a;
        }

        private int vertexIndex_1;
        private float[] vertex3_1 = new float[3];

        private int insertedVertCount_1 = 0;
        private int quadsCount_1 = 0;
        private int quadIndex_1 = 0;
        private Float3Buffer quadVertices_1;

        /// <summary>
        /// ხატავს ზიგზავს
        /// </summary>
        /// <param name="vertices"></param>
        /// <param name="lineWidth"></param>
        public void DrawZigZag(Float3Buffer vertices, float lineWidth)
        {
            zigZag2DBuilder.Reset();

            insertedVertCount_1 = 0;
            vertices.GetInsertedCount(out insertedVertCount_1);

            for(vertexIndex_1 = 0; vertexIndex_1 < insertedVertCount_1; vertexIndex_1++)
            {
                vertices.Get(vertexIndex_1, vertex3_1);
                zigZag2DBuilder.AddVertex(vertex3_1[0], vertex3_1[1]);
            }

            zigZag2DBuilder.ComputeZigZag(lineWidth);

            zigZag2DBuilder.GetQuadsCount(out quadsCount_1);
            
            for(quadIndex_1 = 0; quadIndex_1 < quadsCount_1; quadIndex_1++)
            {
                quadVertices_1 = zigZag2DBuilder.GetQuad(quadIndex_1);
                DrawQuad(quadVertices_1);
            }
        }

        public float[] CreateVertex3DArray(int vertexCount)
        {
            return new float[3 * vertexCount];
        }

        public void SetVertex3D(float[] vertexArray, int vertexIndex, float x, float y, float z)
        {
            int _vertexIndex = vertexIndex * 3;

            vertexArray[_vertexIndex] = x;
            vertexArray[_vertexIndex + 1] = y;
            vertexArray[_vertexIndex + 2] = z;
        }

        /// <summary>
        /// წვეროები ერთითება, საათის ისრმის მიმართლებით
        /// </summary>
        /// <param name="x1"></param>
        /// <param name="y1"></param>
        /// <param name="z1"></param>
        /// <param name="x2"></param>
        /// <param name="y2"></param>
        /// <param name="z2"></param>
        /// <param name="x3"></param>
        /// <param name="y3"></param>
        /// <param name="z3"></param>
        public void DrawTriangle(
            float x1,
            float y1,
            float z1,
            float x2,
            float y2,
            float z2,
            float x3,
            float y3,
            float z3)
        {
            InsertVertex3D(x1, y1, z1);
            InsertVertex3D(x2, y2, z2);
            InsertVertex3D(x3, y3, z3);
        }

        public void DrawTriangle(Vector3[] vertices)
        {
            DrawTriangle(
                vertices[0].x,
                vertices[0].y,
                vertices[0].z,

                vertices[1].x,
                vertices[1].y,
                vertices[1].z,

                vertices[2].x,
                vertices[2].y,
                vertices[2].z);

        }

        /// <summary>
        /// წვეროები ერთითება, საათის ისრმის მიმართლებით
        /// </summary>
        /// <param name="x1"></param>
        /// <param name="y1"></param>
        /// <param name="z1"></param>
        /// <param name="x2"></param>
        /// <param name="y2"></param>
        /// <param name="z2"></param>
        /// <param name="x3"></param>
        /// <param name="y3"></param>
        /// <param name="z3"></param>
        /// <param name="x4"></param>
        /// <param name="y4"></param>
        /// <param name="z4"></param>
        public void DrawQuad(
            float x1,
            float y1,
            float z1,
            float x2, 
            float y2,
            float z2,
            float x3,
            float y3,
            float z3,
            float x4,
            float y4,
            float z4)
        {
            InsertVertex3D(x1, y1, z1);
            InsertVertex3D(x2, y2, z2);
            InsertVertex3D(x3, y3, z3);

            InsertVertex3D(x1, y1, z1);
            InsertVertex3D(x3, y3, z3);
            InsertVertex3D(x4, y4, z4);
        }

        public void DrawQuad2D(float width, float height, Vector3 position)
        {
            var halfWidth = width / 2F;
            var halfHeight = height / 2F;

            DrawQuad(
                position.x - halfWidth,
                position.y + halfHeight,
                position.z,

                position.x + halfWidth,
                position.y + halfHeight,
                position.z,

                position.x + halfWidth,
                position.y - halfHeight,
                position.z,

                position.x - halfWidth,
                position.y - halfHeight,
                position.z);
        }

        private float[] vertex3d_1 = new float[3];

        public void DrawQuad(Float3Buffer quadBuffer)
        {

            quadBuffer.Get(0, vertex3d_1);
            InsertVertex3D(vertex3d_1[0], vertex3d_1[1], vertex3d_1[2]);
            quadBuffer.Get(1, vertex3d_1);
            InsertVertex3D(vertex3d_1[0], vertex3d_1[1], vertex3d_1[2]);
            quadBuffer.Get(2, vertex3d_1);
            InsertVertex3D(vertex3d_1[0], vertex3d_1[1], vertex3d_1[2]);

            quadBuffer.Get(0, vertex3d_1);
            InsertVertex3D(vertex3d_1[0], vertex3d_1[1], vertex3d_1[2]);
            quadBuffer.Get(2, vertex3d_1);
            InsertVertex3D(vertex3d_1[0], vertex3d_1[1], vertex3d_1[2]);
            quadBuffer.Get(3, vertex3d_1);
            InsertVertex3D(vertex3d_1[0], vertex3d_1[1], vertex3d_1[2]);
        }

        public void InsertVertex3D(float x, float y, float z)
        {
            colors[vertexIndex].r = drawColor.r;
            colors[vertexIndex].g = drawColor.g;
            colors[vertexIndex].b = drawColor.b;
            colors[vertexIndex].a = drawColor.a;

            vertices[vertexIndex].Set(x, y, z);

            vertexIndex++;
        }
        
        public void Finish()
        {
            for (var unusedVertexIndex = vertexIndex; unusedVertexIndex < vertices.Length; unusedVertexIndex++)
            {
                vertices[unusedVertexIndex].Set(0F, 0F, 0F);
            }
        }

        public void Render()
        {
            mesh.vertices = vertices;
            mesh.colors = colors;
            mesh.SetIndices(vertexIndices, MeshTopology.Triangles, 0);

            mesh.RecalculateBounds();

            Graphics.DrawMesh(mesh, Matrix4x4.identity, lineMaterial, 0);
        }


    }
}

﻿using System.Collections.Generic;

namespace CoreDopamine
{
    class MessageDispatcher
    {
        public delegate void MessageHandler0();

        private Dictionary<string, List<MessageHandler0>> messageHandlers;

        MessageDispatcher()
        {
            this.messageHandlers = new Dictionary<string, List<MessageHandler0>>();
        }

        public void RegisterMessageHandler(string messageName, MessageHandler0 messageHandler)
        {
            var shellRegisterMessageHandlersList = this.messageHandlers.ContainsKey(messageName);

            const int messageHandlersPreAllocatedCapacity = 8;

            if(shellRegisterMessageHandlersList)
            {
                messageHandlers[messageName] = new List<MessageHandler0>(messageHandlersPreAllocatedCapacity);
            }
            
            var messageHandlerSlotIndex = GetFreeMessageHandlerSlotIndex(messageName);

            messageHandlers[messageName][messageHandlerSlotIndex] = messageHandler;

        }
        
        public void UnregisterMessageHandler(string messageName, MessageHandler0 messageHandler)
        {
            var messageNameExists = messageHandlers.ContainsKey(messageName);

            if(messageNameExists)
            {
                var currentMessageHandlers = messageHandlers[messageName];

                var currentMessageHandlerIndex = currentMessageHandlers.IndexOf(messageHandler);

                currentMessageHandlers[currentMessageHandlerIndex] = null;
            }
        }

        public void PostMessage(string messageName)
        {
            var messageHandlersExist = messageHandlers.ContainsKey(messageName);

            if(messageHandlersExist)
            {

                var currentMessageHandlers = messageHandlers[messageName];
                var messageHandlerSlotsCount = currentMessageHandlers.Count;

                for (var messageHandlerIndex =0; messageHandlerIndex < messageHandlerSlotsCount; messageHandlerIndex++)
                {
                    var messageHandler = currentMessageHandlers[messageHandlerIndex];

                    if(messageHandler!=null)
                    {
                        messageHandler();
                    }
                }

            }
        }

        private int GetFreeMessageHandlerSlotIndex(string messageName)
        {
            int freeSlotIndex = -1;

            var currentMessageHandlers = this.messageHandlers[messageName];

            for(var slotIndex = 0; slotIndex < currentMessageHandlers.Count; slotIndex++)
            {

                if(currentMessageHandlers[slotIndex] == null)
                {
                    freeSlotIndex = slotIndex;
                    break;
                }
                
            }

            var shellAddSlot = freeSlotIndex < 0;

            if(shellAddSlot)
            {
                currentMessageHandlers.Add(null);
                freeSlotIndex = currentMessageHandlers.Count - 1;
            }

            return freeSlotIndex;
        }

    }
}

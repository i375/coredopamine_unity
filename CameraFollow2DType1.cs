﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CoreDopamine
{
    public class CameraFollow2DType1
    {
        public GameObject targetObject;
        public Camera camera;

        public void Update()
        {
            var newPosition = new Vector3(
                targetObject.transform.position.x,
                targetObject.transform.position.y,
                camera.transform.position.z);

            camera.transform.position = newPosition;
        }
    }
}


﻿using UnityEngine;

namespace CoreDopamine
{
    public class SystemRoot : MonoBehaviour
    {
        public Camera mainCamera;

        // Use this for initialization
        private void Start()
        {
#if UNITY_WEBGL
            Application.ExternalCall("HideProgressIndicator");
#endif
            
            DontDestroyOnLoad(this.gameObject);
            
            if(mainCamera != null)
            {
                mainCamera.gameObject.SetActive(true);
            }

            Start_2();
        }

        // Update is called once per frame
        private void Update()
        {
            Update_2();
        }

        // FixedUpdate is called once per physics update
        private void FixedUpdate()
        {
            FixedUpdate_2();
        }

        public virtual void Update_2()
        {

        }

        public virtual void FixedUpdate_2()
        {

        }

        public virtual void Start_2()
        {

        }
    }
}


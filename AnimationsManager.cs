﻿using UnityEngine;
using System.Collections;

namespace CoreDopamine
{
    
    public class AnimationsManager : MonoBehaviour {

        public enum EASING_TYPES
        {
            LINEAR,
            EAS_IN_EAS_OUT
        }

        public class Animation
        {
            AnimationStep AnimationStepCallback;
            AnimationFinished AnimationFinishedCallback;


            EASING_TYPES easingType;

            double from = 0;
            public double From
            {
                get
                {
                    return from;
                }
            }

            double to = 0;
            public double To
            {
                get
                {
                    return to;
                }
            }
            
            bool active;
            public bool Active
            {
                get
                {
                    return active;
                }
            }

            double durationInSeconds;
            public double DurationInSeconds
            {
                get
                {
                    return durationInSeconds;
                }
            }

            double normalizedProgress = 0;
            public double NormalizedProgressEasingApplied
            {
                get
                {
                    switch(easingType)
                    { 
                        case EASING_TYPES.LINEAR:
                            return normalizedProgress;

                        case EASING_TYPES.EAS_IN_EAS_OUT:
                            var easingApplied = (Mathf.Sin((float)normalizedProgress * Mathf.PI - Mathf.PI / 2F) + 1F) / 2F;
                            return easingApplied; 

                        default:
                            return 0F;
                    }
                    
                }
            }

            public double Progress
            {
                get
                {
                    return from + (to - from) * NormalizedProgressEasingApplied;
                }
            }
            
            public void Cancel()
            {
            	active = false;
            }
            
            public void StepAnimation(float deltaTime)
            {
                normalizedProgress += deltaTime / durationInSeconds;

                if(normalizedProgress > 1.0)
                {
                    normalizedProgress = 1.0;

                    AnimationStepCallback(this);
                    AnimationFinishedCallback(this);

                    active = false;
                }
                else
                {
                    AnimationStepCallback(this);
                }

                

            }

            public void Reset(double from, double to, double durationInSeconds, EASING_TYPES easingType, AnimationStep  animationStepHandler, AnimationFinished animationFinishedHandler)
            {
                this.from = from;
                this.to = to;
                this.durationInSeconds = durationInSeconds;
                this.active = true;
                this.AnimationStepCallback = animationStepHandler;
                this.AnimationFinishedCallback = animationFinishedHandler;
                normalizedProgress = 0;
                this.easingType = easingType;


            }
        }

        private Animation[] animationsPool;

        void CreateAnimationsPool(int animationsPoolSize)
        {
            animationsPool = new Animation[animationsPoolSize];

            for(var animationIndex = 0; animationIndex < animationsPoolSize; animationIndex++)
            {
                animationsPool[animationIndex] = new Animation();
            }
        }

        public void QueueAnimation(double from, double to, double durationInSeconds, EASING_TYPES easingType, AnimationStep animationStepHandler, AnimationFinished animationFinishedHandler)
        {
            var animation = TryToGetInactiveAnimation();

            animation.Reset(from, to, durationInSeconds, easingType, animationStepHandler, animationFinishedHandler);
        }

        void StepAnimations(float deltaTime)
        {
            for(var animationIndex = 0; animationIndex < animationsPool.Length; animationIndex++)
            {
                var animation = animationsPool[animationIndex];

                if(animation.Active)
                {
                    animation.StepAnimation(deltaTime);
                }
            }
        }

        private Animation TryToGetInactiveAnimation()
        {
            Animation animation = null;

            for(var animationIndex = 0; animationIndex < animationsPool.Length; animationIndex++)
            {
                animation = animationsPool[animationIndex];

                if(animation.Active == false)
                {
                    break;
                }
            }

            if (animation.Active == true)
            {
                animation = null;
            }

            return animation;
        }


        public int animationsPoolSize = 32;

        void Start()
        {
            CreateAnimationsPool(animationsPoolSize);
        }

        void Update()
        {
            StepAnimations(Time.deltaTime);
        }

        public delegate void AnimationStep(Animation animation);    
        public delegate void AnimationFinished(Animation animation);

    }


}

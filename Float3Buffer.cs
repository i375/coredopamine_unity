﻿using UnityEngine;
using System.Collections;

namespace CoreDopamine
{
    
    public class Float3Buffer
    {

        private float[] float3Buffer;
        private int _index = 0;
        private int insertedCount = 0;

        public Float3Buffer(int count)
        {
            float3Buffer = new float[3 * count];
        }

        public void ResizeIfSmaller(int count)
        {
            if (float3Buffer.Length / 3 < count)
            {
                float3Buffer = new float[3 * count];
            }
        }

        public void Set(int index, float x = 0, float y = 0, float z = 0)
        {
            _index = index * 3;

            float3Buffer[_index] = x;
            float3Buffer[_index + 1] = y;
            float3Buffer[_index + 2] = z;
        }

        public void Get(int index, float[] outFloat3)
        {
            _index = index * 3;

            outFloat3[0] = float3Buffer[_index];
            outFloat3[1] = float3Buffer[_index + 1];
            outFloat3[2] = float3Buffer[_index + 2];
        }

        public void GetCount(out int count)
        {
            count = float3Buffer.Length / 3;
        }

        public void GetInsertedCount(out int insertedCount)
        {
            insertedCount = this.insertedCount / 3;
        }

        public void Reset()
        {
            insertedCount = 0;
        }

        public void Insert(float x = 0, float y = 0, float z = 0)
        {
            float3Buffer[insertedCount] = x;
            float3Buffer[insertedCount + 1] = y;
            float3Buffer[insertedCount + 2] = z;

            insertedCount = insertedCount + 3;
        }

        public float[] GetBuffer()
        {
            return float3Buffer;
        }

    }
    
}

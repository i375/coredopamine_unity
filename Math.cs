﻿using UnityEngine;


namespace CoreDopamine
{
    static class MathU
    {

        /// <summary>
        /// Loops through array index.
        /// 
        /// For example if you have array of 5 elements.
        /// index -1 will return 4
        /// index 5 will return 0
        /// index 6 will return 1
        /// </summary>
        /// <param name="index"></param>
        /// <param name="elementsCount"></param>
        /// <returns></returns>
        public static uint LoopIndex(int index, uint elementsCount)
        {
            var mod = Mod(index, (int)elementsCount);

            return (uint)((mod < 0) ? (elementsCount + mod) : mod);
        }

        public static int Mod(int n, int divisor)
        {
            var mod = n - divisor * Mathf.FloorToInt(n / divisor);

            return mod;
        }

        /// <summary>
        /// Converts boolean to integer
        /// </summary>
        /// <param name="b"></param>
        /// <returns></returns>
        public static int b2i(bool b)
        {
            return b == true ? 1 : 0;
        }
    }
}

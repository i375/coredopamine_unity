﻿using UnityEngine;

namespace CoreDopamine
{
    /// <summary>
    /// ზიგ-ზაგის გეომეტრიის გენერატორი
    /// </summary>
    class ZigZag2DBuilder
    {

        private int insertedVertexCount = 0;

        private Float3Buffer vertices;
        private Float3Buffer vertexVectors;
        private float[] vertexVectorRotationsZ;
        private float[] targetRotationsForRightSide;
        private Float3Buffer rightSideVertexVectors;
        private Float3Buffer rightSideVertexVectorsAngleCorrected;
        private float[] lengthsOfRightSideAngleCorrectedVectors;
        private Float3Buffer rightSideVertexVectorsAngleCorrectedAndNormalized;
        private Float3Buffer rightSideVerticesScaledAndTransformed;
        private Float3Buffer leftSideVerticesScaledAndTransformed;

        /// <summary>
        /// </summary>
        /// <param name="maxVertices">წვეროების მაქსიმალური რაოდენობა</param>
        public ZigZag2DBuilder(int maxVertices)
        {
            InitBuffers(maxVertices);
        }

        private void InitBuffers(int maxVertices)
        {
            vertices = new Float3Buffer(maxVertices);
            vertexVectors = new Float3Buffer(maxVertices);
            vertexVectorRotationsZ = new float[maxVertices];
            targetRotationsForRightSide = new float[maxVertices];
            rightSideVertexVectors = new Float3Buffer(maxVertices);
            rightSideVertexVectorsAngleCorrected = new Float3Buffer(maxVertices);
            lengthsOfRightSideAngleCorrectedVectors = new float[maxVertices];
            rightSideVertexVectorsAngleCorrectedAndNormalized = new Float3Buffer(maxVertices);
            rightSideVerticesScaledAndTransformed = new Float3Buffer(maxVertices);
            leftSideVerticesScaledAndTransformed = new Float3Buffer(maxVertices);
        }

        public void Reset()
        {
            insertedVertexCount = 0;
        }

        public void AddVertex(float x, float y)
        {
            vertices.Set(insertedVertexCount, x, y);
            insertedVertexCount++;
        }

        public void EditVertex(int index, float x, float y)
        {
            if(index > insertedVertexCount-1)
            {
                throw new System.Exception("Trying to edit uninserted vertex value.");
            }

            vertices.Set(index, x, y);
        }

        public void ComputeZigZag(float width)
        {
            if(insertedVertexCount < 3)
            {
                throw new System.Exception("Minimum 3 vertices must be inserted before computing zigzag2d geometry.");
            }

            ActuallyComputeZigZag(width);
        }

        private Float3Buffer zigZagQuad = new Float3Buffer(4);

        /// <summary>
        /// აბრუნებს ტეხილის შემადგენელი უთხკუთხედის 
        /// შემადგენელ წვეროების ბუფერს(4 წვერო).
        /// </summary>
        /// <param name="index"></param>
        /// <returns></returns>
        public Float3Buffer GetQuad(int index)
        {
            zigZagQuad.Reset();

            rightSideVerticesScaledAndTransformed.Get(index + 1, float3_1);
            zigZagQuad.Insert(float3_1[Xi], float3_1[Yi]);

            rightSideVerticesScaledAndTransformed.Get(index, float3_1);
            zigZagQuad.Insert(float3_1[Xi], float3_1[Yi]);

            leftSideVerticesScaledAndTransformed.Get(index, float3_1);
            zigZagQuad.Insert(float3_1[Xi], float3_1[Yi]);

            leftSideVerticesScaledAndTransformed.Get(index + 1, float3_1);
            zigZagQuad.Insert(float3_1[Xi], float3_1[Yi]);

            return zigZagQuad;
        }

        /// <summary>
        /// თვლის რამდენი ოთხკუთხედისგან შედგება ტეხილი
        /// </summary>
        /// <param name="count"></param>
        public void GetQuadsCount(out int count)
        {
            count = insertedVertexCount - 1;
        }

        int index2 = 0;
        float[] float3_1 = new float[3];
        float[] float3_2 = new float[3];
        const float HALF_PI = Mathf.PI / 2F;

        const int Xi = 0;
        const int Yi = 1;
        const int Zi = 2;

        float float_1 = 0;
        
        private void ActuallyComputeZigZag(float width)
        {
            //Computing vertex vectors
            vertexVectors.Set(0);

            for(index2 = 1; index2 < insertedVertexCount; index2++)
            {
                vertices.Get(index2 - 1, float3_1);
                vertices.Get(index2, float3_2);

                float3_1[Xi] = float3_2[Xi] - float3_1[Xi];
                float3_1[Yi] = float3_2[Yi] - float3_1[Yi];

                vertexVectors.Set(index2,
                    float3_1[Xi],
                    float3_1[Yi]);
            }

            // Computing vertex vector rotations Z
            vertexVectors.Get(1, float3_1);

            vertexVectorRotationsZ[0] = Mathf.Atan2(float3_1[Yi], float3_1[Xi]);

            for(index2 = 1; index2 < insertedVertexCount; index2++)
            {
                vertexVectors.Get(index2, float3_1);

                vertexVectorRotationsZ[index2] = Mathf.Atan2(float3_1[Yi], float3_1[Xi]);
            }

            // Computing target rotation for right side
            for(index2 = 0; index2 < insertedVertexCount; index2++)
            {
                targetRotationsForRightSide[index2] = vertexVectorRotationsZ[index2] - HALF_PI;
            }

            // Computing right side vertex vectors
            for(index2 = 0; index2 < insertedVertexCount; index2++)
            {
                rightSideVertexVectors.Set(index2,
                    Mathf.Cos(targetRotationsForRightSide[index2]),
                    Mathf.Sin(targetRotationsForRightSide[index2]));
            }

            // Computing right side vertex vectors angle corrected
            rightSideVertexVectors.Get(0, float3_1);
            rightSideVertexVectorsAngleCorrected.Set(0, float3_1[Xi], float3_1[Yi]);

            for(index2 = 1; index2 < insertedVertexCount - 1; index2++)
            {
                rightSideVertexVectors.Get(index2, float3_1);
                rightSideVertexVectors.Get(index2+1, float3_2);

                rightSideVertexVectorsAngleCorrected.Set(index2,
                    float3_1[Xi] + float3_2[Xi],
                    float3_1[Yi] + float3_2[Yi]);
            }

            rightSideVertexVectors.Get(insertedVertexCount - 1, float3_1);
            rightSideVertexVectorsAngleCorrected.Set(insertedVertexCount - 1, float3_1[Xi], float3_1[Yi]);

            // Computing lengths of righ side angle corrected vectors
            for(index2 = 0; index2 < insertedVertexCount; index2++)
            {
                rightSideVertexVectorsAngleCorrected.Get(index2, float3_1);

                lengthsOfRightSideAngleCorrectedVectors[index2] =
                    Mathf.Sqrt(float3_1[Xi] * float3_1[Xi] + float3_1[Yi] * float3_1[Yi]);
            } 

            // Computing right side vertex vectors angle corrected and normilized
            for(index2 = 0; index2 < insertedVertexCount; index2++)
            {
                float_1 = lengthsOfRightSideAngleCorrectedVectors[index2];
                rightSideVertexVectorsAngleCorrected.Get(index2, float3_1);

                rightSideVertexVectorsAngleCorrectedAndNormalized.Set(index2,
                   float3_1[Xi] / float_1,
                   float3_1[Yi] / float_1);
            }

            // Computing right side vertices scaled and transformed
            for(index2 = 0; index2 < insertedVertexCount; index2++)
            {
                vertices.Get(index2, float3_1);
                rightSideVertexVectorsAngleCorrectedAndNormalized.Get(index2, float3_2);

                rightSideVerticesScaledAndTransformed.Set(index2,
                    float3_1[Xi] + float3_2[Xi] * width,
                    float3_1[Yi] + float3_2[Yi] * width);
            }

            // Computing left side vertices scaled and transformed
            for (index2 = 0; index2 < insertedVertexCount; index2++)
            {
                vertices.Get(index2, float3_1);
                rightSideVertexVectorsAngleCorrectedAndNormalized.Get(index2, float3_2);

                leftSideVerticesScaledAndTransformed.Set(index2,
                    float3_1[Xi] + float3_2[Xi] * (-1 * width),
                    float3_1[Yi] + float3_2[Yi] * (-1 * width) );
            }

        }
    }
}

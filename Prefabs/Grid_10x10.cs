﻿using UnityEngine;

namespace CoreDopamine
{
    public class Grid_10x10 : MonoBehaviour
    {
        public Camera targetCamera2D;

        // Update is called once per frame
        void Update()
        {
            var cameraPos = targetCamera2D.transform.position;

            transform.position = new Vector3(Mathf.Floor(cameraPos.x), Mathf.Floor(cameraPos.y), transform.position.z);
        }
    }
}

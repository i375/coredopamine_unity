﻿using UnityEngine;

namespace CoreDopamine
{

    public class Collider2DEventsEmiter : MonoBehaviour
    {

        public enum ColliderEventType
        {
            Enter,
            Stay,
            Exit
        }

        public interface Collider2DEventsReceiver
        {
            void Collider2DEvent(Collision2D collision, ColliderEventType colliderEventType);
        }

        private Collider2DEventsReceiver eventsReceiver = null;

        public GameObject eventsTarget;

        public void Start()
        {
            if(eventsTarget != null)
            {
                SetColliderEventsReceiver(((Collider2DEventsReceiver)(object)eventsTarget));
            }
        }

        public void SetColliderEventsReceiver(Collider2DEventsReceiver eventsReceiver)
        {
            this.eventsReceiver = eventsReceiver;
        }

        public void OnCollisionEnter2D(Collision2D collision)
        {
            if (eventsReceiver != null)
            {
                eventsReceiver.Collider2DEvent(collision, ColliderEventType.Enter);
            }
        }

        public void OnCollisionStay2D(Collision2D collision)
        {
            if (eventsReceiver != null)
            {
                eventsReceiver.Collider2DEvent(collision, ColliderEventType.Stay);
            }
        }

        public void OnCollisionExit2D(Collision2D collision)
        {
            if (eventsReceiver != null)
            {
                eventsReceiver.Collider2DEvent(collision, ColliderEventType.Exit);
            }
        }
    }

}
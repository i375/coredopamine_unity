﻿using UnityEngine;
using System.Collections;

using System.Collections.Generic;

namespace USharp
{
        
    public class HiResScreenShots : MonoBehaviour {
        public List<Vector2> screenshotResolutions;

        private bool takeHiResShot = false;
        
        public static string ScreenShotName(int width, int height, int screenShotNumber) {
            return string.Format("{0}/screenshots/{1}screen_{2}x{3}_{4}.png",                             
                                Application.dataPath, 
                                screenShotNumber,
                                width, height, 
                                System.DateTime.Now.ToString("yyyy-MM-dd_HH-mm-ss"));
        }
        
        public void TakeHiResShot() {
            takeHiResShot = true;
        }

        IEnumerator RenderHiResScreenshots()
        {
            yield return new WaitForEndOfFrame();

            int screenShotCounter = 0;

            foreach(var screenshotResolution in screenshotResolutions)
            {
                int resWidth = (int)screenshotResolution.x;
                int resHeight = (int)screenshotResolution.y;

                RenderTexture rt = new RenderTexture(resWidth, resHeight, 24);
                Camera.main.targetTexture = rt;
                Texture2D screenShot = new Texture2D(resWidth, resHeight, TextureFormat.RGB24, false);
                Camera.main.Render();
                RenderTexture.active = rt;
                screenShot.ReadPixels(new Rect(0, 0, resWidth, resHeight), 0, 0);
                Camera.main.targetTexture = null;
                RenderTexture.active = null; // JC: added to avoid errors
                Destroy(rt);
                byte[] bytes = screenShot.EncodeToPNG();
                string filename = ScreenShotName(resWidth, resHeight, screenShotCounter);
                System.IO.File.WriteAllBytes(filename, bytes);
                Debug.Log(string.Format("Took screenshot to: {0}", filename));

                screenShotCounter++;
            }


        }
        
        void LateUpdate() {
            takeHiResShot |= Input.GetKeyDown(KeyCode.P);
            
            if(takeHiResShot)
            {
                StartCoroutine(RenderHiResScreenshots());

                takeHiResShot = false;
            }
        }
    }


}
